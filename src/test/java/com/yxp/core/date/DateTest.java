package com.yxp.core.date;

import java.util.Date;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Administrator
 * @date 2018年8月1日下午5:22:32
 * @version v1.0.0
 * @since jdk1.8
 * 
 * <p>日期类Date测试
 */
@Slf4j
public class DateTest {
	
	@Test
	public void createDate() {
		Date date = new Date();
		log.info("{}",date.getTime());
	}

}
