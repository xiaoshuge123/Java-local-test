package com.yxp.core.date;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Administrator
 * @date 2018年8月1日下午5:06:02
 * @version v1.0.0
 * @since jdk1.8
 * <p>Java8引入的新日期类的测试类
 */
@Slf4j
public class NewDateAPITest {
	
	/**
	 * LocalDate.now()获取当前的日期,不包含时间
	 */
	@Test
	public void getTodayDate() {
		LocalDate date = LocalDate.now();
		log.info("今天的日期是:{}",date);
		date = date.plusDays(2);
		log.info("{}天后的日期是:{}",2,date);
	}
	
	@Test
	public void localTimeTest() {
		LocalTime time = LocalTime.now();
		log.info("{}",time);
	}

}
