package com.yxp.core.reflect.app;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import org.junit.Test;

import lombok.Data;

/**
 * @author Administrator
 * @date 2018年8月6日下午4:24:26
 * 
 * <p>内省测试类</p>
 */
public class IntrospectionTest {
	
	@Data
	private class Student{
		private String id;
		private String name;
		private int age;
		private Date birthday;
	}
	

	/**
	 * @throws IntrospectionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * <p>操作单个属性</p>
	 */
	@Test
	public void operateField() throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		Student student = new Student();
		PropertyDescriptor pd = new PropertyDescriptor("name", Student.class);
		Method set = pd.getWriteMethod();
		set.invoke(student,"yxp");
		Method get = pd.getReadMethod();
		System.out.println(get.invoke(student, null));
	}
	
	@Test
	public void getBeansInfo() throws IntrospectionException {
//		PropertyDescriptor pd = new PropertyDescriptor("name", Student.class);
		BeanInfo beanInfo = Introspector.getBeanInfo(Student.class);
		
		PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
		if (pds != null && pds.length > 0) {
			for(PropertyDescriptor pd : pds) {
				System.out.println(pd);
			}
		}
	}
	
}
