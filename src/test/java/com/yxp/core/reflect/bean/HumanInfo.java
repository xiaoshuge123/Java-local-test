package com.yxp.core.reflect.bean;

import java.util.Date;

import lombok.Data;

@Data
public abstract class HumanInfo {

	
	private String name;
	private int age;
	private Date birthday;
	
}
