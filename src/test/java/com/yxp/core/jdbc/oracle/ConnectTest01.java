package com.yxp.core.jdbc.oracle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectTest01 {
	
	private static final String URL = "jdbc:oracle:thin:@192.168.241.132:1521:XE";
	private static final String USERNAME = "yxptest";
	private static final String PASSWORD = "root";

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		Connection connection = getConnection();
		System.out.println(connection.getSchema());
	}
	
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

}
