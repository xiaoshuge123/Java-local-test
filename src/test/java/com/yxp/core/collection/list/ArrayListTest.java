package com.yxp.core.collection.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ArrayListTest {
	
	private class MyTest_01{
		
		private List<String> list_01 = Arrays.asList("yxp","zll","wyy");
		private List<String> list_02 = Arrays.asList("czx","lmx","fzy");
		@Override
		public String toString() {
			return "MyTest_01 " + list_01 + " " + list_02;
		}
		
		
		
	}
	
	@Test
	public void printList() {
		List<String> list_01 = Arrays.asList("yxp","zll","wyy");
		List<String> list_02 = Arrays.asList("czx","lmx","fzy");
		
		List<List<String>> ls = new ArrayList<List<String>>();
		ls.add(list_01);
		ls.add(list_02);
		System.out.println(ls);
		
	}
	
	@Test
	public void runMyTest_01() {
		System.out.println(new MyTest_01());
		this.getClass().getFields();
	}
	

}
