package com.yxp.core.collection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SetTest {
	
	@Test
	public void getSetValue() {
		Set<String> sets = new HashSet<>();
		sets.add("1");
		sets.add("2");
		for(String set : sets) {
			log.info(set);
		}
	}
	
	@Test
	public void getEntrySetValue() {
		
		Map<String, String> map = new HashMap<>();
		map.put("1", "test_01");
		map.put("2", "test_02");
		map.put("3", "test_03");
		Iterator<Entry<String, String>> iterator = map.entrySet().iterator();
		while(iterator.hasNext()) {
			log.info("{}",iterator.next());
		}
		
	}

}
