package com.yxp.core.timer;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Administrator
 * <p>在Java中,一个完整的定时任务由Timer和TimerTask两个类来配合完成,API中的定义:
 * 	<ul>
 * 		<li>Timer:一种工具,线程用其安排以后在后台线程中执行的任务,可安排任务执行一次,也可重复执行</li>
 * 		<li>TimerRask:Timer安排为执行一次或者重复执行的任务</li>
 * 		<li>总结:Timer是一种定时器工具,用来在后台线程计划执行指定任务,TimerTask是一个抽象类,他的子类代表一个可以被Timer计划的任务</li>
 * 	</ul>
 * 
 *
 */
@Slf4j
public class TimerTest {
	
	
	private class MyTimerTask extends TimerTask{
		
		private String msg;
		
		private MyTimerTask(String msg){
			this.msg = msg;
		}
		
		private void excute() {
			log.info("{}",msg);
		}
		
		@Override
		public void run() {
			excute();
		}
	}
	
	private Timer timer;
	
	@Before
	public void init() {
		log.info("执行任务");
		timer = new Timer();
	}
	
	
	/**
	 * @throws InterruptedException
	 * <p>指定延迟时间执行指定任务
	 */
	@Test
	public void delayPrintMsg() throws InterruptedException {
		
		timer.schedule(new MyTimerTask("3秒后执行"), 3000);
		TimeUnit.SECONDS.sleep(3);
		
	}
	
	/**
	 * @throws InterruptedException 
	 * <p>在指定时间执行定时任务:定时在当前时间的5秒钟后执行
	 */
	@Test
	public void assignPrintMsg() throws InterruptedException {
		
		timer.schedule(new MyTimerTask("在当前时间的指定5秒钟后的时间执行事件"),new Date(new Date().getTime()+5000));
		TimeUnit.SECONDS.sleep(6);
	}
	
	/**
	 * @throws InterruptedException 
	 * <p>在延迟指定时间后以指定的间隔时间循环执行定时任务
	 */
	@Test
	public void delayPrintMsgInCycleTime() throws InterruptedException {
		timer.schedule(new MyTimerTask("3秒后每隔1秒执行一次任务"), 3000, 1000);
		TimeUnit.SECONDS.sleep(20);
	}
	
	/**
	 * @throws InterruptedException 
	 * <p>在指定时间后以指定的间隔时间循环执行定时任务
	 */
	@Test
	public void assignPrintMsgInCycleTime() throws InterruptedException {
		timer.schedule(new MyTimerTask("在当前时间的指定5秒钟后的时间每隔1秒执行一次任务"),new Date(new Date().getTime()+5000),1000);
		TimeUnit.SECONDS.sleep(20);
	}

}
