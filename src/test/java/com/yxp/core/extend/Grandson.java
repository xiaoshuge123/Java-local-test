package com.yxp.core.extend;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Grandson extends Child {

	@Override
	public void onCall() {
		log.info("{} 调用 onCall方法","grandson");
	}
	
	/*@Override
	public void afterOnCall() {
		log.info("{} 调用 afterOnCall方法","grandson");
	}*/
	
}
