package com.yxp.core.extend;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Parent {
	
	public void call() {
		log.info("{} 调用 Call方法",this.getClass().getSimpleName());
		onCall();
		afterOnCall();
	}
	
	public void onCall() {
		log.info("{} 调用 onCall方法","parent");
	}
	
	public void afterOnCall() {
		log.info("{} 调用 afterOnCall方法","parent");
	}

}
