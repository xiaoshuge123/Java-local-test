package com.yxp.core.extend;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author Administrator
 * <p>继承相关测试类
 */
@Slf4j
public class ExtendTest {
	
	/**
	 * <h3>多态测试</h3>
	 * 
	 * <p>Child类继承自Parent类,并且重写了onCall方法,现在创建一个child对象,调用call方法,查看结果是调用父类的onCall还是子类的onCall
	 * 
	 * <p>结果:多态,父类变量引用的是具体的子类对象,因此,会先默认调用子类重写的方法
	 */
	@Test
	public void onCall() {
		Parent parent = new Parent();
		Parent child = new Child();
		Parent grandson = new Grandson();
		
		log.info("========== parent call ==========");
		parent.call();
		log.info("========== child call ==========");
		child.call();
		log.info("========== grandson call ==========");
		grandson.call();
	}

}
