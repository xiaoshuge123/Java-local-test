package com.yxp.core.extend;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Child extends Parent{

	@Override
	public void onCall() {
		log.info("{} 调用 onCall方法","child");
	}
	
	@Override
	public void afterOnCall() {
		log.info("{} 调用 afterOnCall方法","child");
	}
	
}
