package com.yxp.core.path;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;


import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class ProjectPathTest {

	private String pkName = "com.yxp.core.path".replaceAll("\\.","/");
	
	/**
	 * @throws MalformedURLException 
	 * 
	 * <p>
	 * 获取当前项目的根路径下的webapp文件夹的url
	 * @throws URISyntaxException 
	 * 
	 */
	@Test
	public void getProjectPath_01() throws URISyntaxException {
		
		String path = System.getProperty("user.dir");
		String url = String.format("%s/%s", path,"webapp");
		File file = new File(url);
		log.info("{}",file.exists());
		
	}
	
	@Test
	public void getProjectPath_02() {
		String projectPath = new File("").getAbsolutePath();
		log.info(projectPath);
	}

	@Test
	public void getClassPath_01(){
		String cPath = this.getClass().getResource("/").getPath()+pkName;
		log.info("{}",cPath);
	}

	@Test
	public void getClassPath_02(){
		String cPath = this.getClass().getClassLoader().getResource("").getPath();
		log.info("{}",cPath);
	}

	@Test
	public void getClassPath_03(){
		URL url = this.getClass().getClassLoader().getResource("");
		log.info("{}",url);
	}

}
