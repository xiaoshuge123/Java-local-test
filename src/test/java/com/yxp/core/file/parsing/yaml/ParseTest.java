package com.yxp.core.file.parsing.yaml;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;



public class ParseTest {
	
	private Map<String, Object> records;
	
	@Before
	public void init() {
		Yaml application_properties = new Yaml();
		records = application_properties.load(this.getClass().getClassLoader().getResourceAsStream("application.yml"));
		System.out.println(records);
	}
	
	@Test
	public void test_01() {
		System.out.println(getValue("test"));
	}

	//简单实现了Spring框架的@Value功能
	private Object getValue(String path) {
		String[] keys = path.split("\\.");
		Map<String,Object> tempRecords = records;
		Object result = null;
		if (keys.length == 0) {
			result = records.get(path);
		}else {
			for(int i = 0 ; i < keys.length; i++) {
				Object record = tempRecords.get(keys[i]);
				if (record instanceof Map) {
					tempRecords = (Map<String, Object>) record;
				}else {
					result = record;
				}
			}
		}
		return result;
	}

}
